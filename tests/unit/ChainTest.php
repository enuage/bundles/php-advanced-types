<?php

use Codeception\Test\Unit;
use Enuage\Type\Chain;

/**
 * Class ChainTest
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class ChainTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testChain()
    {
        $chain = new Chain();
        $values = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'];
        shuffle($values);

        foreach ($values as $value) {
            $chain->addNode($value);
        }

        $this->assertFalse($chain->isEmpty());
        $this->assertEquals($values, $chain->getChainValues());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testChainNode()
    {
        $chain = new Chain();
        $chain->addNode('a')->addNode('b')->addNode('c');

        $this->assertEquals('c', $chain->getLastNode()->getValue());
        $this->assertEquals('a', $chain->getRootNode()->getValue());
        $this->assertEquals('b', $chain->getLastNode()->getPreviousNode()->getValue());
        $this->assertEquals('b', $chain->getRootNode()->getNextNode()->getValue());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testTitledNode()
    {
        $this->tester->expectThrowable(
            RuntimeException::class,
            function () {
                $chain = new Chain();
                $chain->addNode('a', 'test')->addNode('b', 'test');
            }
        );

        $chain = new Chain();
        $chain->addNode('a')->addNode('b', 'target')->addNode('c');
        $this->assertEquals('a', $chain->getNodeByTitle('target')->getPreviousNode()->getValue());
        $this->assertEquals('c', $chain->getNodeByTitle('target')->getNextNode()->getValue());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveNode()
    {
        $chain = new Chain();
        $chain->addNode('a')->addNode('b', 'target')->addNode('c');
        $this->assertEquals(['a'], $chain->removeNode('target')->getChainValues());

        $chain = new Chain();
        $chain->addNode('a', 'target')->addNode('b')->addNode('c');
        $this->assertEquals([], $chain->removeNode('target')->getChainValues());
    }
}
