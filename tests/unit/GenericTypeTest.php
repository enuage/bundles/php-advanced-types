<?php /** @noinspection PhpParamsInspection */
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Tests\Type;

use Codeception\Test\Unit;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\PseudoGeneric;
use Enuage\Type\Helper\Type;
use Exception;
use stdClass;
use UnitTester;

/**
 * Class GenericTypeTest
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class GenericTypeTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testCreation()
    {
        $this->tester->expectThrowable(Exception::class, function () {
            new PseudoGeneric(Type::STRING_TYPE, 'void');
        });

        $expected = [
            new AdvancedArrayObject([1]),
            new AdvancedArrayObject([2]),
            new AdvancedArrayObject([3]),
        ];

        $this->assertSame($expected, (new PseudoGeneric(AdvancedArrayObject::class, Type::INTEGER_TYPE, $expected))->toArray());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testExplode()
    {
        $array = PseudoGeneric::explode(',', 'a,b,c');
        $this->assertEquals(['a', 'b', 'c'], $array->getArrayCopy());
    }


    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveElement()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $array->removeElement('a');

        $this->assertSame([1 => 'b', 2 => 'c'], $array->getArrayCopy());

        $array->removeElement('b', true);

        $this->assertSame(['c'], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveElements()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $array->removeElements('a', 'b');
        $this->assertSame([2 => 'c'], $array->getArrayCopy());

        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $array->removeElements('a', 'b')->resetKeys();
        $this->assertSame(['c'], $array->getArrayCopy());

        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $array->removeElements(...['a', 'b']);
        $this->assertSame([2 => 'c'], $array->getArrayCopy());

        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $array->removeElements(...['a', 'b'])->resetKeys();
        $this->assertSame(['c'], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveByKey()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $array->removeByKey('a');
        $this->assertEquals(['b' => 1, 'c' => 2], $array->getArrayCopy());

        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $array->removeByKey(0, true);
        $this->assertEquals(['b', 'c'], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveByKeys()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $array->removeByKeys('a', 'b');
        $this->assertEquals(['c' => 2], $array->getArrayCopy());

        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $array->removeByKeys(...['a', 'b']);
        $this->assertEquals(['c' => 2], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testContainsKey()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);

        $this->assertTrue($array->containsKey('a'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testContainsKeys()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);

        $this->assertTrue($array->containsKeys('a', 'b'));
        $this->assertTrue($array->containsKeys(...['a', 'b']));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testContainsElement()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);

        $this->assertTrue($array->containsElement('a'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testContainsElements()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);

        $this->assertTrue($array->containsElements(['a', 'b'], PseudoGeneric::MODE_CONTAINS_STRICT));
        $this->assertTrue($array->containsElements(['a', 'd'], PseudoGeneric::MODE_CONTAINS_EASY));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGetKeys()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);

        $this->assertEquals(['a', 'b', 'c'], $array->getKeys());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGetValues()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $this->assertEquals([0, 1, 2], $array->getValues());

        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $this->assertEquals([0, 1], $array->getKeysValues('a', 'b'));

        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $this->assertEquals([0, 1], $array->getKeysValues(...['a', 'b']));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testSet()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $array->set('a', 4);

        $this->assertEquals(['a' => 4, 'b' => 1, 'c' => 2], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testAdd()
    {
        $array = new PseudoGeneric(Type::SCALAR_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $array->add('d');
        $this->assertEquals(['a' => 0, 'b' => 1, 'c' => 2, 'd'], $array->getArrayCopy());

        $array = new PseudoGeneric(Type::SCALAR_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $array->add('d', 'e');
        $this->assertEquals(['a' => 0, 'b' => 1, 'c' => 2, 'd', 'e'], $array->getArrayCopy());

        $array = new PseudoGeneric(Type::SCALAR_TYPE, Type::STRING_TYPE, ['a' => 0, 'b' => 1, 'c' => 2]);
        $array->add(...['d', 'e']);
        $this->assertEquals(['a' => 0, 'b' => 1, 'c' => 2, 'd', 'e'], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGetStringValue()
    {
        $array = new PseudoGeneric(Type::SCALAR_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c', 0]);

        $this->assertEquals('a', $array->getStringValue(0));
        $this->assertEquals('0', $array->getStringValue(3));
        $this->assertEquals('undefined', $array->getStringValue(4, 'undefined'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGet()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);

        $this->assertEquals('a', $array->get(0));
        $this->assertEquals(null, $array->get(3));

        $this->tester->expectThrowable(Exception::class, function () use ($array) {
            $array->get(3, 2);
        });
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testIntValue()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 1, 'b' => 2, 'c' => 3]);

        $this->assertEquals(1, $array->getIntValue('a'));
        $this->assertEquals(4, $array->getIntValue('d', 4));
        $this->assertEquals(null, $array->getIntValue('d'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testFloatValue()
    {
        $array = new PseudoGeneric(Type::FLOAT_TYPE, Type::STRING_TYPE, ['a' => 1.1, 'b' => 2.2, 'c' => 3.3]);

        $this->assertEquals(1.1, $array->getFloatValue('a'));
        $this->assertEquals(4.4, $array->getFloatValue('d', 4.4));
        $this->assertEquals(null, $array->getFloatValue('d'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testBooleanValue()
    {
        $array = new PseudoGeneric(Type::BOOLEAN_TYPE, Type::STRING_TYPE, ['a' => true, 'b' => true, 'c' => true]);

        $this->assertEquals(true, $array->getBooleanValue('a'));
        $this->assertEquals(true, $array->getBooleanValue('d', true));
        $this->assertEquals(false, $array->getBooleanValue('d'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testAppend()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a']);
        $array->append('b');
        $array->append(['c', 'd'], true);
        $array->append(['e', ['f']], true);

        $this->assertEquals(['a', 'b', 'c', 'd', 'e', 'f'], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testIsEmptyValue()
    {
        $array = new PseudoGeneric(Type::MIXED_TYPE, Type::STRING_TYPE,
                                   [
                'a' => 1,
                'b' => null,
                'c' => '',
                'd' => [],
                'e' => new PseudoGeneric(Type::INTEGER_TYPE),
                'f' => new AdvancedArrayObject(),
                'g' => new stdClass(),
            ]
        );

        $this->assertFalse($array->isEmptyValue('a'));
        $this->assertTrue($array->isEmptyValue('b'));
        $this->assertTrue($array->isEmptyValue('c'));
        $this->assertTrue($array->isEmptyValue('d'));
        $this->assertTrue($array->isEmptyValue('e'));
        $this->assertTrue($array->isEmptyValue('f'));
        $this->assertTrue($array->isEmptyValue('g'));
        $this->assertTrue($array->isEmptyValue('h'));
    }

    public function testIsEmpty()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE);

        $this->assertTrue($array->isEmpty());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testToString()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);

        $this->assertEquals(PseudoGeneric::class . '@' . spl_object_hash($array), $array->__toString());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testPush()
    {
        $array = new PseudoGeneric(Type::INTEGER_TYPE);

        $array->push(1);
        $array->push(2);
        $array->push(3);

        $this->assertEquals([1 => 1, 2 => 2, 3 => 3], $array->toArray());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testPushMixed()
    {
        $array = new PseudoGeneric(Type::SCALAR_TYPE, Type::SCALAR_TYPE, ['a' => 'a', 'b' => 'b']);

        $array->push(1);
        $array->push(2);
        $array->push(3);

        $this->assertEquals(['a' => 'a', 'b' => 'b', 1 => 1, 2 => 2, 3 => 3], $array->toArray());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGetDifferenceArray()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $this->assertEquals(['d'], $array->getDifference(['a', 'b', 'c', 'd'], false));

        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $this->assertEquals([], $array->getDifference(['a', 'b', 'c'], false));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGetDifferenceObject()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $this->assertEquals(new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['d']), $array->getDifference(['a', 'b', 'c', 'd']));

        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $this->assertEquals(new PseudoGeneric(), $array->getDifference(['a', 'b', 'c']));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGetNextByKey()
    {
        $array = new PseudoGeneric(Type::STRING_TYPE, Type::INTEGER_TYPE, ['a', 'b', 'c']);
        $this->assertEquals('c', $array->getNext(1));

        $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assertEquals(3, $array->getNext('b'));

        $this->tester->expectThrowable(Exception::class, function () {
            $array = new PseudoGeneric(Type::INTEGER_TYPE, Type::STRING_TYPE, ['a' => 1, 'b' => 2, 'c' => 3]);
            $array->getNext(2);
        });
    }
}
