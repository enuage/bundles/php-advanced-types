<?php

declare(strict_types=1);

namespace Enuage\Tests\Type\unit;

use ArrayObject;
use Codeception\Test\Unit;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\PseudoEnum;
use Exception;
use stdClass;
use UnitTester;

/**
 * Class PseudoEnumTest
 * @package Enuage\Tests\Type
 */
class PseudoEnumTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpParamsInspection
     */
    public function testEnumConstruction()
    {
        $enum = new PseudoEnum(['a', 'b', 'c']);
        $this->assertEquals(new AdvancedArrayObject(['a', 'b', 'c']), $enum->getElements());

        $enum = new PseudoEnum(new AdvancedArrayObject(['a', 'b', 'c']));
        $this->assertEquals(new AdvancedArrayObject(['a', 'b', 'c']), $enum->getElements());

        $enum = new PseudoEnum(new ArrayObject(['a', 'b', 'c']));
        $this->assertEquals(new AdvancedArrayObject(['a', 'b', 'c']), $enum->getElements());

        $this->tester->expectThrowable(Exception::class, function () {
            new PseudoEnum(1);
        });

        $this->tester->expectThrowable(Exception::class, function () {
            new PseudoEnum(null);
        });

        $this->tester->expectThrowable(Exception::class, function () {
            new PseudoEnum(3.14);
        });

        $this->tester->expectThrowable(Exception::class, function () {
            new PseudoEnum(new stdClass());
        });

        $this->tester->expectThrowable(Exception::class, function () {
            new PseudoEnum(true);
        });
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpParamsInspection
     */
    public function testEnumValidateArray()
    {
        $enum = new PseudoEnum(['a', 'b', 'c']);
        $this->assertTrue($enum->validate(['a', 'b', 'c']));

        $this->tester->expectThrowable(Exception::class, function () use ($enum) {
            $enum->validate(['a', 'b', 'c', 'd']);
        });

        $this->tester->expectThrowable(Exception::class, function () use ($enum) {
            $enum->validate(false);
        });

        $this->tester->expectThrowable(Exception::class, function () use ($enum) {
            $enum->validate(null);
        });

        $this->tester->expectThrowable(Exception::class, function () use ($enum) {
            $enum->validate(3);
        });

        $this->tester->expectThrowable(Exception::class, function () use ($enum) {
            $enum->validate(3.14);
        });

        $enum = new PseudoEnum(new AdvancedArrayObject(['a', 'b', 'c']));
        $this->assertTrue($enum->validate(['a', 'b', 'c']));

        $enum = new PseudoEnum(new ArrayObject(['a', 'b', 'c']));
        $this->assertTrue($enum->validate(['a', 'b', 'c']));

        $enum = new PseudoEnum(new ArrayObject(['a', 'b', 'c']));
        $this->assertTrue($enum->validate(new ArrayObject(['a', 'b', 'c'])));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testEnumContainsValue()
    {
        $enum = new PseudoEnum(['a', 'b', 3]);

        foreach (['a' => true, 'b' => true, 3 => true, 'd' => false] as $value => $expectation) {
            $this->assertEquals($expectation, $enum->containsValue($value));
        }
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFromArray()
    {
        $this->assertTrue(PseudoEnum::fromArray(['a', 'b', 'c'])->containsValue('a'));
    }
}
