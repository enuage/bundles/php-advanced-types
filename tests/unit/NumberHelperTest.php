<?php

namespace Enuage\Tests\Type\unit;

use Codeception\Test\Unit;
use Enuage\Type\Helper\NumberHelper;

/**
 * Class NumberHelperTest
 * @package Enuage\Tests\Type\unit
 */
class NumberHelperTest extends Unit
{
    /** @noinspection PhpUnhandledExceptionInspection */
    public function testInRange()
    {
        $this->assertTrue(NumberHelper::inRange(2, 1, 3));
        $this->assertTrue(NumberHelper::inRange(3, 2.71, 3.14));
        $this->assertTrue(NumberHelper::inRange(2.9, 2.71, 3.14));
        $this->assertTrue(NumberHelper::inRange(rand(1, 10), 1, 10));
        $this->assertFalse(NumberHelper::inRange(4, 1, 3));
    }
}
