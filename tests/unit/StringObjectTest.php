<?php /** @noinspection PhpParamsInspection */

/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Tests\Type;

use Codeception\Test\Unit;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\StringObject;
use InvalidArgumentException;
use stdClass;
use UnitTester;

/**
 * Class StringObjectTest
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class StringObjectTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testInvalidArgument()
    {
        $this->tester->expectThrowable(
            InvalidArgumentException::class,
            function () {
                new StringObject([]);
            }
        );

        $this->tester->expectThrowable(
            InvalidArgumentException::class,
            function () {
                new StringObject(new stdClass());
            }
        );

        $this->tester->expectThrowable(
            InvalidArgumentException::class,
            function () {
                new StringObject(
                    new class {
                    }
                );
            }
        );
    }

    public function testConvertibleObject()
    {
        $arrayType = new AdvancedArrayObject([]);
        $this->assertEquals(
            $arrayType->__toString(),
            (new StringObject($arrayType))->getValue()
        );
    }

    public function testConvertToSnakeCase()
    {
        $inputs = [
            'helloWorld',
            'hello_World',
            'Hello_world',
            'HelloWorld',
        ];

        foreach ($inputs as $input) {
            $string = new StringObject($input);
            $this->assertEquals('hello_world', $string->toSnakeCase());
        }

        $mixedInputs = [
            'hello2World',
            'hello2_World',
            'Hello2_world',
            'Hello2World',
        ];

        foreach ($mixedInputs as $mixedInput) {
            $mixedString = new StringObject($mixedInput);
            $this->assertEquals('hello2_world', $mixedString->toSnakeCase());
        }

        $otherCases = [
            'kebab-case' => 'kebab_case',
            'PascalCase' => 'pascal_case',
            'camelCase' => 'camel_case',
            'snake_case' => 'snake_case',
        ];

        foreach ($otherCases as $actual => $expected) {
            $string = new StringObject($actual);
            $this->assertEquals($expected, $string->toSnakeCase());
        }
    }

    public function testCamelCase()
    {
        $strings = [
            'kebab-case' => 'kebabCase',
            'PascalCase' => 'pascalCase',
            'camelCase' => 'camelCase',
            'snake_case' => 'snakeCase',
            'hello2_world' => 'hello2World',
            'hello2_World' => 'hello2World',
            'hello2-World' => 'hello2World',
            'hello2-world' => 'hello2World',
        ];

        foreach ($strings as $actual => $expected) {
            $string = new StringObject($actual);
            $this->assertEquals($expected, $string->toCamelCase());
        }
    }

    public function testPascalCase()
    {
        $strings = [
            'kebab-case' => 'KebabCase',
            'PascalCase' => 'PascalCase',
            'camelCase' => 'CamelCase',
            'snake_case' => 'SnakeCase',
            'hello2_world' => 'Hello2World',
            'hello2_World' => 'Hello2World',
            'hello2-World' => 'Hello2World',
            'hello2-world' => 'Hello2World',
        ];

        foreach ($strings as $actual => $expected) {
            $string = new StringObject($actual);
            $this->assertEquals(
                $expected,
                $string->toCamelCase(StringObject::MODE_CAMEL_CASE_PASCAL)
            );
        }
    }

    public function testKebabCase()
    {
        $strings = [
            'kebab-case' => 'kebab-case',
            'PascalCase' => 'pascal-case',
            'camelCase' => 'camel-case',
            'snake_case' => 'snake-case',
            'hello2_world' => 'hello2-world',
            'hello2_World' => 'hello2-world',
            'hello2-World' => 'hello2-world',
            'hello2-world' => 'hello2-world',
        ];

        foreach ($strings as $actual => $expected) {
            $string = new StringObject($actual);
            $this->assertEquals($expected, $string->toKebabCase());
        }
    }

    public function testPrepend()
    {
        $this->assertEquals(
            'prefixTest',
            (new StringObject('Test'))->prepend('prefix')->getValue()
        );
    }

    public function testIsEndsWith()
    {
        $this->assertTrue(
            (new StringObject('testPostfix'))->isEndsWith('Postfix')
        );

        $this->assertFalse(
            (new StringObject())->isEndsWith('Postfix')
        );
    }

    public function testIsEmpty()
    {
        $this->assertTrue((new StringObject(''))->isEmpty());
        $this->assertTrue((new StringObject())->isEmpty());
    }

    public function testIsStartsWith()
    {
        $this->assertTrue(
            (new StringObject('prefixedTest'))->isStartsWith('prefixed')
        );

        $this->assertFalse(
            (new StringObject())->isStartsWith('prefixed')
        );
    }

    public function testIsEqualTo()
    {
        $this->assertTrue((new StringObject('test'))->isEqualTo('test'));
    }

    public function testAppend()
    {
        $this->assertEquals(
            'testModified',
            (new StringObject('test'))->append('Modified')->getValue()
        );
    }

    public function testReset()
    {
        $this->assertEquals(
            'test',
            (new StringObject('test'))->append('Modified')->reset()
        );
    }

    public function testReplace()
    {
        $this->assertEquals(
            'tezt',
            (new StringObject('test'))->replace('s', 'z')->getValue()
        );
    }

    public function testExplode()
    {
        $parts = (new StringObject('a,b,c,,d'))->explode(
            ',',
            StringObject::MODE_REMOVE_EMPTY_VALUES
        );

        $this->assertInstanceOf(AdvancedArrayObject::class, $parts);
        $this->assertEquals(4, $parts->count());
    }
}
