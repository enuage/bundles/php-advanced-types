<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Tests\Type;

use Codeception\Test\Unit;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\ArrayHelper;
use RuntimeException;
use SplQueue;
use stdClass;
use function json_decode;

/**
 * Class ArrayHelperTest
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class ArrayHelperTest extends Unit
{
    public function testIsIterable()
    {
        $this->assertTrue(ArrayHelper::isIterable(['a', 'b', 'c']));
        $this->assertTrue(ArrayHelper::isIterable(new AdvancedArrayObject(['a', 'b', 'c'])));
    }

    public function testIsTrueForAll()
    {
        $predicate = function (&$value) {
            return 0 === $value % 2;
        };

        $this->assertTrue(ArrayHelper::isTrueForAll(['a' => 2, 'b' => 4, 'c' => 6], $predicate));
        $this->assertTrue(ArrayHelper::isTrueForAll(new AdvancedArrayObject(['a' => 2, 'b' => 4, 'c' => 6]), $predicate));
        $this->assertFalse(ArrayHelper::isTrueForAll(new AdvancedArrayObject(['a' => 2, 'b' => 3, 'c' => 5]), $predicate));

        $this->expectException(RuntimeException::class);
        $this->assertTrue(ArrayHelper::isTrueForAll(2, $predicate));
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testToArray()
    {
        $init = ['a' => 1, 'b' => 2, 'c' => 3];

        $notArray1 = new stdClass();
        $notArray1->a = 1;
        $notArray1->b = 2;
        $notArray1->c = 3;

        $notArray2 = new AdvancedArrayObject($init);

        $notArray3 = json_decode('{"a":1, "b":2, "c":3}');

        foreach ([$notArray1, $notArray2, $notArray3] as $notArray) {
            $array = ArrayHelper::toArray($notArray);
            $this->assertIsArray($array);
            $this->assertEquals($init, $array);

            $arrayObject = ArrayHelper::toArray($notArray, true);
            $this->assertTrue($arrayObject instanceof AdvancedArrayObject);
            $this->assertEquals($init, $arrayObject->toArray());
        }

        $this->expectException(RuntimeException::class);
        ArrayHelper::toArray(false);
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testMerge()
    {
        $this->assertIsArray(ArrayHelper::merge([], ['a'], new AdvancedArrayObject(['b']), new SplQueue(), new stdClass()));

        $this->expectException(RuntimeException::class);
        $this->assertIsArray(ArrayHelper::merge(null, ['a'], new AdvancedArrayObject(['b']), new SplQueue(), new stdClass()));
    }
}
