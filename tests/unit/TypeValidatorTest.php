<?php

declare(strict_types=1);

namespace Enuage\Tests\Type\unit;

use ArrayAccess;
use ArrayObject;
use Codeception\Test\Unit;
use Countable;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Element\OrderedElementInterface;
use Enuage\Type\Helper\Type;
use Enuage\Type\Throwable\InvalidTypeException;
use Enuage\Type\Validator\TypeValidator;
use Exception;
use stdClass;
use UnitTester;

/**
 * Class TypeValidatorTest
 */
class TypeValidatorTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testIsValidClass()
    {
        $this->assertTrue(TypeValidator::isValid(AdvancedArrayObject::class, new AdvancedArrayObject()));
        $this->assertTrue(TypeValidator::isValid(Countable::class, new AdvancedArrayObject()));
        $this->assertFalse(TypeValidator::isValid(OrderedElementInterface::class, new AdvancedArrayObject()));
    }

    public function testIsValidNumber()
    {
        $this->assertTrue(TypeValidator::isValid(Type::NUMBER_TYPE, 1));
        $this->assertTrue(TypeValidator::isValid(Type::NUMBER_TYPE, 3.14));
        $this->assertTrue(TypeValidator::isValid(Type::NUMBER_TYPE, '3'));
        $this->assertTrue(TypeValidator::isValid(Type::NUMBER_TYPE, '3.14'));
        $this->assertFalse(TypeValidator::isValid(Type::NUMBER_TYPE, true));
        $this->assertFalse(TypeValidator::isValid(Type::NUMBER_TYPE, null));
        $this->assertFalse(TypeValidator::isValid(Type::NUMBER_TYPE, 'string'));
    }

    public function testIsValidArrayObject()
    {
        $this->assertTrue(TypeValidator::isValid(Type::ARRAY_OBJECT_TYPE, []));
        $this->assertTrue(TypeValidator::isValid(Type::ARRAY_OBJECT_TYPE, new AdvancedArrayObject()));
        $this->assertTrue(TypeValidator::isValid(Type::ARRAY_OBJECT_TYPE, new ArrayObject()));
        $this->assertTrue(TypeValidator::isValid(Type::ARRAY_OBJECT_TYPE, new stdClass()));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_OBJECT_TYPE, 1));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_OBJECT_TYPE, 'string'));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_OBJECT_TYPE, null));
        $this->assertTrue(TypeValidator::isValid(Type::ARRAYOBJECT_TYPE, new ArrayObject()));
    }

    public function testIsValidArray()
    {
        $this->assertTrue(TypeValidator::isValid(Type::ARRAY_TYPE, []));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_TYPE, new AdvancedArrayObject()));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_TYPE, new ArrayObject()));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_TYPE, new stdClass()));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_TYPE, 1));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_TYPE, 'string'));
        $this->assertFalse(TypeValidator::isValid(Type::ARRAY_TYPE, null));
    }

    public function testIsValidObject()
    {
        $this->assertTrue(TypeValidator::isValid(Type::OBJECT_TYPE, new AdvancedArrayObject()));
        $this->assertTrue(TypeValidator::isValid(Type::OBJECT_TYPE, new ArrayObject()));
        $this->assertTrue(TypeValidator::isValid(Type::OBJECT_TYPE, new stdClass()));
        $this->assertFalse(TypeValidator::isValid(Type::OBJECT_TYPE, []));
        $this->assertFalse(TypeValidator::isValid(Type::OBJECT_TYPE, 1));
        $this->assertFalse(TypeValidator::isValid(Type::OBJECT_TYPE, 'string'));
        $this->assertFalse(TypeValidator::isValid(Type::OBJECT_TYPE, null));
    }

    public function testIsValidCallable()
    {
        $this->assertTrue(TypeValidator::isValid(Type::CALLBACK_TYPE, function () {}));
        $this->assertTrue(TypeValidator::isValid(Type::CALLABLE_TYPE, function () {}));
        $this->assertFalse(TypeValidator::isValid(Type::CALLBACK_TYPE, null));
        $this->assertFalse(TypeValidator::isValid(Type::CALLABLE_TYPE, null));
    }

    public function testIsValidNull()
    {
        $this->assertTrue(TypeValidator::isValid(Type::NULL_TYPE, null));
        $this->assertFalse(TypeValidator::isValid(Type::NULL_TYPE, 'string'));
        $this->assertFalse(TypeValidator::isValid(Type::NULL_TYPE, 1));
    }

    public function testIsValidBoolean()
    {
        $this->assertTrue(TypeValidator::isValid(Type::BOOLEAN_TYPE, true));
        $this->assertTrue(TypeValidator::isValid(Type::BOOLEAN_TYPE, false));
        $this->assertTrue(TypeValidator::isValid(Type::BOOLEAN_TYPE, 'yes'));
        $this->assertTrue(TypeValidator::isValid(Type::BOOLEAN_TYPE, 1));
        $this->assertFalse(TypeValidator::isValid(Type::BOOLEAN_TYPE, 'yes', true));
        $this->assertFalse(TypeValidator::isValid(Type::BOOLEAN_TYPE, 1, true));
    }

    public function testIsCountable()
    {
        $this->assertTrue(TypeValidator::isValid(Type::COUNTABLE_TYPE, []));
        $this->assertTrue(TypeValidator::isValid(Type::COUNTABLE_TYPE, new AdvancedArrayObject()));
        $this->assertFalse(TypeValidator::isValid(Type::COUNTABLE_TYPE, null));
        $this->assertFalse(TypeValidator::isValid(Type::COUNTABLE_TYPE, 3.14));
        $this->assertFalse(TypeValidator::isValid(Type::COUNTABLE_TYPE, json_encode([])));
    }

    /** @noinspection PhpDeprecationInspection */
    public function testArrayValidation()
    {
        $this->tester->expectThrowable(Exception::class, function () {
            TypeValidator::validateArray(Type::INTEGER_TYPE, [-1, 1, 2.71, 3.14]);
        });
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpDeprecationInspection
     */
    public function testIsValidTypes()
    {
        $this->assertTrue(TypeValidator::isValidTypes(['string'], 'test'));
        $this->assertTrue(TypeValidator::isValidTypes(['integer', 'string'], 1));
        $this->assertTrue(TypeValidator::isValidTypes([ArrayAccess::class], new ArrayObject()));
        $this->assertFalse(TypeValidator::isValidTypes(['integer', 'string'], false));
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testAnyOf()
    {
        $this->assertTrue(TypeValidator::anyOf('string', 'a'));
        $this->assertTrue(TypeValidator::anyOf('boolean', 'yes'));
        $this->assertFalse(TypeValidator::anyOf('boolean', 'yes', true, true));

        $this->assertTrue(TypeValidator::anyOf(['integer', 'string'], '1'));
        $this->assertFalse(TypeValidator::anyOf(['integer', 'float'], '1', true, true));

        $this->assertTrue(TypeValidator::anyOf('string', ['a', 'b', 'c'], true, true, true));
        $this->assertTrue(TypeValidator::anyOf(['integer', 'string'], ['a', 'b', 1], true, true, true));
        $this->assertFalse(TypeValidator::anyOf('integer', ['a', 'b', 'c'], true, true, true));

        $this->tester->expectThrowable(InvalidTypeException::class, function () {
            TypeValidator::anyOf('integer', 'a', false);
        });
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testEachOf()
    {
        $this->assertTrue(TypeValidator::eachOf('string', ['a', 'b', 'c']));
        $this->assertFalse(TypeValidator::eachOf('string', ['a', 'b', 1]));

        $this->assertTrue(TypeValidator::eachOf('integer', [1, 2, 3]));
        $this->assertFalse(TypeValidator::eachOf('integer', [1, 2, 'a']));

        $this->assertTrue(TypeValidator::eachOf('string|integer', [1, 2, 'a']));

        $this->tester->expectThrowable(InvalidTypeException::class, function () {
            TypeValidator::eachOf('integer', [1, 2, 'a'], false);
        });

        $this->tester->expectThrowable(Exception::class, function () {
            /** @noinspection PhpParamsInspection */
            TypeValidator::eachOf('integer', 2, false);
        });
    }

    public function testIsValidJson()
    {
        $this->assertTrue(TypeValidator::isValid('json', '{"a":"b"}'));
        $this->assertFalse(TypeValidator::isValid('json', 'Hello world'));
        $this->assertFalse(TypeValidator::isValid('json', '{"a":"b",}'));
        $this->assertFalse(TypeValidator::isValid('json', 2));
    }
}
