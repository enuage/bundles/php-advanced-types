<?php /** @noinspection PhpParamsInspection */
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Tests\Type;

use Codeception\Test\Unit;
use Enuage\Tests\_data\OrderedQueueElement;
use Enuage\Type\QueueOrdered;
use InvalidArgumentException;
use RuntimeException;
use function shuffle;

/**
 * Class QueueOrderedTest
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class QueueOrderedTest extends Unit
{
    /**
     * @var QueueOrdered
     */
    private $queue;

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */

    public function testOrderedQueueMaxOrderNumber()
    {
        $this->createOrder();
        $this->assertEquals(7, $this->queue->getMaxOrderNumber());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */

    private function createOrder()
    {
        $this->queue = new QueueOrdered([
            new OrderedQueueElement(3, 3),
            new OrderedQueueElement(1, 1),
            new OrderedQueueElement(4),
            new OrderedQueueElement(6, 6),
            new OrderedQueueElement(2, 2),
            new OrderedQueueElement(7),
            new OrderedQueueElement(5, 5),
        ]);
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */

    public function testRightOrder()
    {
        $this->createOrder();
        $this->queue->order();

        $i = 1;
        /** @var OrderedQueueElement $element */
        foreach ($this->queue as $element) {
            $this->assertEquals($element->getExpectedOrderNumber(), $element->getOrderNumber());
            $this->assertEquals($i, $element->getOrderNumber());

            $i++;
        }
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */

    public function testMissedOrderElement()
    {
        $this->createRandomOrder();
        $this->queue->order();

        $i = 1;
        /** @var OrderedQueueElement $element */
        foreach ($this->queue as $element) {
            $this->assertEquals($element->getExpectedOrderNumber(), $element->getOrderNumber());
            if (2 !== $i) {
                $this->assertEquals($i, $element->getOrderNumber());
                $i++;
            }
        }

        $this->createRandomOrder();
        $this->queue->setOrderDirection(QueueOrdered::ORDER_DESC);
        $this->queue->order();

        $i = 9;
        /** @var OrderedQueueElement $element */
        foreach ($this->queue as $element) {
            if (2 !== $i) {
                $this->assertEquals($i, $element->getOrderNumber());
                $i--;
            }
        }
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */

    private function createRandomOrder()
    {
        $elements = [
            new OrderedQueueElement(3, 3),
            new OrderedQueueElement(1, 1),
            new OrderedQueueElement(9, 9),
            new OrderedQueueElement(4, 4),
            new OrderedQueueElement(6, 6),
            new OrderedQueueElement(8, 8),
            new OrderedQueueElement(5, 5),
            new OrderedQueueElement(7, 7),
        ];

        shuffle($elements);

        $this->queue = new QueueOrdered($elements);
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */

    public function testInvalidQueueArgument()
    {
        $queue = new QueueOrdered();
        $this->expectException(InvalidArgumentException::class);
        $queue->enqueue(null);
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testInvalidQueueElement()
    {
        $queue = new QueueOrdered();

        $queue->order(); // Nothing happens

        $queue->enqueue(new OrderedQueueElement(1));
        $queue->offsetSet(0, 'invalid data');

        $this->expectException(RuntimeException::class);
        $queue->getMaxOrderNumber();
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testOrderDescending()
    {
        $this->createOrder();
        $this->queue->setOrderDirection(QueueOrdered::ORDER_DESC);
        $this->queue->order();

        $i = 7;
        /** @var OrderedQueueElement $element */
        foreach ($this->queue as $element) {
            $this->assertEquals($i, $element->getOrderNumber());

            $i--;
        }
    }

    public function testIterate()
    {
        $this->createOrder();
        $tester = clone $this;
        $this->queue->iterate(function ($element) use ($tester) {
            $tester->assertTrue($element instanceof OrderedQueueElement);
        });

        (new QueueOrdered())->iterate(function ($element) {
            // Nothing happens
        });
    }
}
