<?php /** @noinspection PhpParamsInspection */

/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Tests\Type;

use Codeception\Test\Unit;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\StringObject;
use RuntimeException;
use stdClass;

/**
 * Class AdvancedArrayObjectTest
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class AdvancedArrayObjectTest extends Unit
{
    public function testExplode()
    {
        $array = AdvancedArrayObject::explode(',', 'a,b,c');

        $this->assertEquals(['a', 'b', 'c'], $array->getArrayCopy());
    }

    public function testToArray()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertEquals($array->toArray(), $array->getArrayCopy());
    }

    public function testFirst()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertEquals('a', $array->first());
    }

    public function testLast()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertEquals('c', $array->last());
    }

    public function testKey()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertEquals(0, $array->key());
    }

    public function testNext()
    {
        $elements = ['a', 'b', 'c'];
        $array = new AdvancedArrayObject($elements);

        while (true) {
            $arrayNext = $array->next();
            $elementsNext = next($elements);

            if (!$arrayNext || $elementsNext) {
                break;
            }

            $this->assertEquals($elementsNext, $arrayNext);
        }
    }

    public function testCurrent()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertEquals('a', $array->current());

        $array = new AdvancedArrayObject([3.4, 1, 'a']);

        $this->assertEquals(3.4, $array->current());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveElement()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $array->removeElement('a');

        $this->assertSame([1 => 'b', 2 => 'c'], $array->getArrayCopy());

        $array->removeElement('b', true);

        $this->assertSame(['c'], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveElements()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $array->removeElements('a', 'b');
        $this->assertSame([2 => 'c'], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $array->removeElements('a', 'b')->resetKeys();
        $this->assertSame(['c'], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $array->removeElements(...['a', 'b']);
        $this->assertSame([2 => 'c'], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $array->removeElements(...['a', 'b'])->resetKeys();
        $this->assertSame(['c'], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveByKey()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $array->removeByKey('a');
        $this->assertEquals(['b' => 1, 'c' => 2], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $array->removeByKey(0, true);
        $this->assertEquals(['b', 'c'], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testRemoveByKeys()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $array->removeByKeys('a', 'b');
        $this->assertEquals(['c' => 2], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $array->removeByKeys(...['a', 'b']);
        $this->assertEquals(['c' => 2], $array->getArrayCopy());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testContainsKey()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);

        $this->assertTrue($array->containsKey('a'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testContainsKeys()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);

        $this->assertTrue($array->containsKeys('a', 'b'));
        $this->assertTrue($array->containsKeys(...['a', 'b']));
        $this->assertFalse($array->containsKeys(...['d', 'e']));
    }

    public function testContainsElement()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertTrue($array->containsElement('a'));
    }

    public function testContainsElements()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertTrue($array->containsElements(['a', 'b'], AdvancedArrayObject::MODE_CONTAINS_STRICT));
        $this->assertTrue($array->containsElements(['a', 'd'], AdvancedArrayObject::MODE_CONTAINS_EASY));
    }

    public function testExists()
    {
        $array = new AdvancedArrayObject(['a' => 2, 'b' => 4, 'c' => 3]);

        $this->assertTrue(
            $array->exists(
                static function ($value) {
                    return 0 === $value % 3;
                }
            )
        );

        $this->assertFalse(
            $array->exists(
                static function ($value) {
                    return 0 === $value % 5;
                }
            )
        );
    }

    public function testGetKeys()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);

        $this->assertEquals(['a', 'b', 'c'], $array->getKeys());
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGetValues()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $this->assertEquals([0, 1, 2], $array->getValues());

        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $this->assertEquals([0, 1], $array->getKeysValues('a', 'b'));

        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $this->assertEquals([0, 1], $array->getKeysValues(...['a', 'b']));
    }

    public function testSet()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $array->set('a', 4);

        $this->assertEquals(['a' => 4, 'b' => 1, 'c' => 2], $array->getArrayCopy());
    }

    public function testAdd()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $array->add('d');
        $this->assertEquals(['a' => 0, 'b' => 1, 'c' => 2, 'd'], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $array->add('d', 'e');
        $this->assertEquals(['a' => 0, 'b' => 1, 'c' => 2, 'd', 'e'], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $array->add(...['d', 'e']);
        $this->assertEquals(['a' => 0, 'b' => 1, 'c' => 2, 'd', 'e'], $array->getArrayCopy());
    }

    public function testMap()
    {
        $power = function ($x) {
            return $x ** 2;
        };

        $array = new AdvancedArrayObject(['a' => 1, 'b' => 2, 'c' => 3]);
        $mapped = $array->map($power);
        $this->assertEquals(['a' => 1, 'b' => 4, 'c' => 9], $mapped->getArrayCopy());

        $array = new AdvancedArrayObject(['a' => 1, 'b' => 2, 'c' => 3]);
        $array->map($power, true);
        $this->assertEquals(['a' => 1, 'b' => 4, 'c' => 9], $array->getArrayCopy());
    }

    public function testFilter()
    {
        $closure = function ($x) {
            return $x & 1;
        };

        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $filtered = $array->filter($closure);
        $this->assertEquals(['b' => 1,], $filtered->getArrayCopy());

        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $array->filter($closure, true);
        $this->assertEquals(['b' => 1,], $array->getArrayCopy());
    }

    public function testIsTrueForAll()
    {
        $predicate = function (&$value) {
            return 0 === $value % 2;
        };

        $array = new AdvancedArrayObject(['a' => 2, 'b' => 4, 'c' => 6]);
        $this->assertTrue($array->isTrueForAll($predicate));

        $array = new AdvancedArrayObject(['a' => 2, 'b' => 3, 'c' => 5]);
        $this->assertFalse($array->isTrueForAll($predicate));
    }

    public function testClear()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $array->clear();

        $this->assertEmpty($array->getArrayCopy());
    }

    public function testSlice()
    {
        $array = new AdvancedArrayObject(['a' => 0, 'b' => 1, 'c' => 2]);
        $sliced = $array->slice(0, 1);

        $this->assertEquals(['a' => 0], $sliced->getArrayCopy());

        $array->slice(0, 1, true);

        $this->assertEquals(['a' => 0], $array->getArrayCopy());
    }

    public function testGetNext()
    {
        $elements = ['a', 'b', 'c'];
        $array = new AdvancedArrayObject($elements);

        for ($i = 0; $i <= 3; $i++) {
            $nextIndex = $i + 1;
            $expected = $nextIndex >= count($array) ? null : $elements[$nextIndex];
            $this->assertEquals($expected, $array->getNext($i));
        }
    }

    public function testGetNextByKey()
    {
        $elements = ['a', 'b', 'c'];
        $array = new AdvancedArrayObject($elements);

        $this->assertEquals('c', $array->getNext(1));

        $elements = ['a' => 1, 'b' => 2, 'c' => 3];
        $array = new AdvancedArrayObject($elements);

        $this->assertEquals('3', $array->getNext('b'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGetStringValue()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c', 0]);

        $this->assertEquals('a', $array->getStringValue(0));
        $this->assertEquals('0', $array->getStringValue(3));
        $this->assertEquals('undefined', $array->getStringValue(4, 'undefined'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testGet()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertEquals('a', $array->get(0));
        $this->assertEquals(null, $array->get(3));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testIntValue()
    {
        $array = new AdvancedArrayObject(['a' => 1, 'b' => 2, 'c' => 3]);

        $this->assertEquals(1, $array->getIntValue('a'));
        $this->assertEquals(4, $array->getIntValue('d', 4));
        $this->assertEquals(null, $array->getIntValue('d'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testFloatValue()
    {
        $array = new AdvancedArrayObject(['a' => 1.1, 'b' => 2.2, 'c' => 3.3]);

        $this->assertEquals(1.1, $array->getFloatValue('a'));
        $this->assertEquals(4.4, $array->getFloatValue('d', 4.4));
        $this->assertEquals(null, $array->getFloatValue('d'));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testBooleanValue()
    {
        $array = new AdvancedArrayObject(['a' => true, 'b' => true, 'c' => true]);

        $this->assertEquals(true, $array->getBooleanValue('a'));
        $this->assertEquals(true, $array->getBooleanValue('d', true));
        $this->assertEquals(false, $array->getBooleanValue('d'));
    }

    public function testAppend()
    {
        $array = new AdvancedArrayObject(['a']);
        $array->append('b');
        $array->append(['c', 'd'], true);
        $array->append(['e', ['f']], true);

        $this->assertEquals(['a', 'b', 'c', 'd', 'e', 'f'], $array->getArrayCopy());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testIsEmptyValue()
    {
        $array = new AdvancedArrayObject(
            [
                'a' => 1,
                'b' => null,
                'c' => '',
                'd' => [],
                'e' => new AdvancedArrayObject(),
                'g' => new stdClass(),
            ]
        );

        $this->assertFalse($array->isEmptyValue('a'));
        $this->assertTrue($array->isEmptyValue('b'));
        $this->assertTrue($array->isEmptyValue('c'));
        $this->assertTrue($array->isEmptyValue('d'));
        $this->assertTrue($array->isEmptyValue('e'));
        $this->assertTrue($array->isEmptyValue('f'));
        $this->assertTrue($array->isEmptyValue('g'));
    }

    public function testIsEmpty()
    {
        $array = new AdvancedArrayObject();

        $this->assertTrue($array->isEmpty());
    }

    public function testFromIterator()
    {
        $a = new AdvancedArrayObject(['a', 'b', 'c']);
        $b = AdvancedArrayObject::fromIterator($a->getIterator());

        $this->assertEquals($a, $b);
    }

    public function testToString()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);

        $this->assertEquals(AdvancedArrayObject::class.'@'.spl_object_hash($array), $array->__toString());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFromArray()
    {
        $init = ['a' => 1, 'b' => 2, 'c' => 3];
        $expected = ['x' => 1, 'y' => 2, 'z' => 3];

        $this->assertEquals(new AdvancedArrayObject($init), AdvancedArrayObject::fromArray($init));
        $this->assertEquals(new AdvancedArrayObject($init), AdvancedArrayObject::fromArray($init, ['a', 'b', 'c']));
        $this->assertEquals(
            new AdvancedArrayObject(['a' => 1, 'b' => 2]),
            AdvancedArrayObject::fromArray($init, ['a', 'b'])
        );
        $this->assertEquals(
            new AdvancedArrayObject($expected),
            AdvancedArrayObject::fromArray($init, ['a', 'b', 'c'], ['x', 'y', 'z'])
        );
        $this->assertEquals(
            new AdvancedArrayObject(['x' => 1, 'y' => 2, 'c' => 3]),
            AdvancedArrayObject::fromArray($init, ['a', 'b', 'c'], ['x', 'y'])
        );
        $this->assertEquals(
            new AdvancedArrayObject(['x' => 1, 'y' => 2]),
            AdvancedArrayObject::fromArray($init, ['a', 'b'], ['x', 'y', 'z'])
        );

        $this->expectException(RuntimeException::class);
        AdvancedArrayObject::fromArray(1);
    }

    public function testPush()
    {
        $array = new AdvancedArrayObject();

        $array->push(1);
        $array->push(2);
        $array->push(3);

        $this->assertEquals([1 => 1, 2 => 2, 3 => 3], $array->toArray());
    }

    public function testPushMixed()
    {
        $array = new AdvancedArrayObject(['a' => 'a', 'b' => 'b']);

        $array->push(1);
        $array->push(2);
        $array->push(3);

        $this->assertEquals(['a' => 'a', 'b' => 'b', 1 => 1, 2 => 2, 3 => 3], $array->toArray());
    }

    public function testGetMaxKey()
    {
        $array1 = new AdvancedArrayObject(['a', 'b', 'c']);
        $this->assertEquals(2, $array1->getMaxKey());

        $array1 = new AdvancedArrayObject(['a' => 'a', 'b' => 'b', 'c' => 'c']);
        $this->assertEquals('c', $array1->getMaxKey());
    }

    public function testGetDifferenceArray()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $this->assertEquals(['d'], $array->getDifference(['a', 'b', 'c', 'd'], false));

        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $this->assertEquals([], $array->getDifference(['a', 'b', 'c'], false));
    }

    public function testGetDifferenceObject()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $this->assertEquals(new AdvancedArrayObject(['d']), $array->getDifference(['a', 'b', 'c', 'd']));

        $array = new AdvancedArrayObject(['a', 'b', 'c']);
        $this->assertEquals(new AdvancedArrayObject(), $array->getDifference(['a', 'b', 'c']));
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function testImplode()
    {
        $this->assertEquals('a,b,c', AdvancedArrayObject::fromArray(['a', 'b', 'c'])->implode(','));
        $this->assertInstanceOf(
            StringObject::class,
            AdvancedArrayObject::fromArray(['a', 'b', 'c'])->implode(',', true)
        );
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testImport()
    {
        $array = new AdvancedArrayObject(['a', 'b', 'c', 'g']);
        $array->import(['d', 'e', 'f']);
        $this->assertEquals(['d', 'e', 'f', 'g'], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a', 'b', 'c', 'g']);
        $array->import(['d', 'e', 'f'], false);
        $this->assertEquals(['a', 'b', 'c', 'g', 'd', 'e', 'f'], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a' => 1, 'b' => 2, 'c' => 3]);
        $array->import(['c' => 3, 'd' => 4, 'e' => 5, 'f' => 6], false);
        $this->assertEquals(['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 'f' => 6], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a' => 1, 'b' => 2, 'c' => 3]);
        $array->import(['c' => 7, 'd' => 4, 'e' => 5, 'f' => 6], true);
        $this->assertEquals(['a' => 1, 'b' => 2, 'c' => 7, 'd' => 4, 'e' => 5, 'f' => 6], $array->getArrayCopy());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testKeyValueDuplication()
    {
        $array = new AdvancedArrayObject(['a' => 1]);
        $array->duplicateValueForKey('a', 'b');

        $this->assertEquals(['a' => 1, 'b' => 1], $array->getArrayCopy());

        $array = new AdvancedArrayObject(['a' => 1]);
        $array->duplicateValueForKeys('a', ['b', 'c']);

        $this->assertEquals(['a' => 1, 'b' => 1, 'c' => 1], $array->getArrayCopy());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testIterate()
    {
        $array = [1 => 2, 2 => 4, 3 => 6];
        AdvancedArrayObject::fromArray($array)->iterate(function ($value, $key) {
            $this->assertEquals($value, $key * 2);
        });
    }
}
