<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Tests\_data;

use Enuage\Type\Element\OrderedElement;

/**
 * Class OrderedQueueElement
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class OrderedQueueElement extends OrderedElement
{
    /**
     * @var int
     */
    private $expectedOrderNumber;

    /**
     * OrderedQueueElement constructor.
     *
     * @param int $expectedOrderNumber
     * @param int $orderNumber
     */
    public function __construct(int $expectedOrderNumber, int $orderNumber = null)
    {
        $this->expectedOrderNumber = $expectedOrderNumber;

        if (null !== $orderNumber) {
            parent::setOrderNumber($orderNumber);
        }
    }

    /**
     * @return int
     */
    public function getExpectedOrderNumber(): int
    {
        return $this->expectedOrderNumber;
    }
}
