<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type;

use Enuage\Type\Element\ChainNode;
use Enuage\Type\Helper\Type;
use Exception;
use RuntimeException;

use function array_reverse;

/**
 * Class Chain
 *
 * @package Enuage\Type
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class Chain
{
    /**
     * @var PseudoGeneric<string,ChainNode>
     */
    protected $nodes;

    /**
     * @var ChainNode|null
     */
    protected $lastNode;

    /**
     * Chain constructor.
     */
    public function __construct()
    {
        $this->nodes = new PseudoGeneric(ChainNode::class, Type::STRING_TYPE);
    }

    /**
     * @param $value
     * @param string|null $title
     *
     * @return Chain
     *
     * @throws Exception
     */
    public function addNode($value, string $title = null): Chain
    {
        $lastNode = $this->getLastNode();
        $node = new ChainNode($value, $lastNode);

        if (null !== $title) {
            if (true === $this->getNodes()->containsKey($title)) {
                throw new RuntimeException('Node with title "'.$title.'" already exists.');
            }

            $node->setTitle($title);
        }

        if (null !== $this->getLastNode()) {
            $this->nodes->removeByKey($lastNode->getTitle() ?? $lastNode->getIdentifier());
            $lastNode->setNextNode($node);
            $this->nodes->set($lastNode->getTitle() ?? $lastNode->getIdentifier(), $lastNode);
        }

        $this->getNodes()->set($node->getTitle() ?? $node->getIdentifier(), $node);
        $this->lastNode = $node;

        $this->order();

        return $this;
    }

    /**
     * @return ChainNode|null
     */
    public function getLastNode()
    {
        return $this->lastNode;
    }

    /**
     * @return PseudoGeneric
     */
    public function getNodes(): PseudoGeneric
    {
        return $this->nodes;
    }

    /**
     * @return $this
     *
     * @throws Exception
     */
    public function order()
    {
        $nodes = [];
        if ($node = $this->getLastNode()) {
            while (false === $node->isRoot()) {
                $nodes[$node->getTitle() ?? $node->getIdentifier()] = $node;
                $node = $node->getPreviousNode();
            }

            $nodes[$node->getTitle() ?? $node->getIdentifier()] = $node;
        }

        $this->nodes->recreate(array_reverse($nodes));

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->nodes->isEmpty();
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function getChainValues(): array
    {
        return $this->getNodes()->map(
            function (ChainNode $node) {
                return $node->getValue();
            }
        )->getValues();
    }

    /**
     * @return ChainNode|null
     */
    public function getRootNode()
    {
        return $this->getNodes()->filter(
            function (ChainNode $node) {
                return true === $node->isRoot();
            }
        )->first();
    }

    /**
     * @param string $title
     *
     * @return Chain
     *
     * @throws Exception
     */
    public function removeNode(string $title): Chain
    {
        if ($node = $this->getNodeByTitle($title)) {
            $this->lastNode = $node->isRoot() ? null : $node->getPreviousNode();
            ($node->isRoot() ? $node : $node->getPreviousNode())->removeNextNode(
                function (ChainNode $node) {
                    $this->getNodes()->removeByKey($node->getTitle() ?? $node->getIdentifier());
                }
            );

            $this->order();
        }

        return $this;
    }

    /**
     * @param string $title
     *
     * @return ChainNode|null
     *
     * @throws Exception
     */
    public function getNodeByTitle(string $title)
    {
        return $this->getNodes()->get($title);
    }
}
