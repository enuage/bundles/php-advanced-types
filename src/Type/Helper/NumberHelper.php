<?php

namespace Enuage\Type\Helper;

use Enuage\Type\Validator\TypeValidator;
use Exception;

/**
 * Class NumberHelper
 * @package Enuage\Type\Helper
 */
class NumberHelper
{
    /**
     * @param int|float|double $value
     * @param int|float|double $minValue
     * @param int|float|double $maxValue
     *
     * @return bool
     *
     * @throws Exception
     */
    public static function inRange($value, $minValue, $maxValue): bool
    {
        TypeValidator::eachOf(Type::NUMBER_TYPE, [$value, $minValue, $maxValue]);

        return $minValue <= $value && $maxValue >= $value;
    }
}
