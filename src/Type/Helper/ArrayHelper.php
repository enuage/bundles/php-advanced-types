<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type\Helper;

use Closure;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Throwable\InvalidTypeException;
use Enuage\Type\Validator\TypeValidator;
use Exception;
use IteratorAggregate;
use RuntimeException;

use function array_merge;
use function get_object_vars;
use function is_object;

/**
 * Class ArrayHelper
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class ArrayHelper
{
    /**
     * Tests whether the given predicate holds for all elements of this array.
     *
     * @param $elements
     * @param Closure $predicate
     *
     * @return bool
     */
    public static function isTrueForAll($elements, Closure $predicate): bool
    {
        if (!self::isIterable($elements)) {
            throw new RuntimeException('Provided elements for check are not an iterable data type.');
        }
        foreach ($elements as $key => $value) {
            if (!$predicate($value, $key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * This method replace PHP built-in method `is_iterable` because it requires PHP >= 7.1.0.
     *
     * @param $value
     *
     * @return bool
     *
     * @throws InvalidTypeException
     */
    public static function isIterable($value): bool
    {
        return TypeValidator::anyOf(Type::ITERABLE_TYPE, $value);
    }

    /**
     * @param mixed ...$values
     *
     * @return array
     * @throws Exception
     *
     */
    public static function merge(...$values): array
    {
        foreach ($values as &$value) {
            $value = self::toArray($value);
        }

        return array_merge($values);
    }

    /**
     * @param $value
     * @param bool $asObject
     *
     * @return array|AdvancedArrayObject
     *
     * @throws RuntimeException
     * @throws Exception
     */
    public static function toArray($value, bool $asObject = false)
    {
        if ($value instanceof IteratorAggregate) {
            $value = $value->getIterator()->getArrayCopy();
        }

        if (is_object($value)) {
            $value = get_object_vars($value);
        }

        if (!ArrayHelper::isIterable($value)) {
            throw new RuntimeException('Provided argument for method "toArray()" is not iterable: '.serialize($value));
        }

        if (true === $asObject) {
            return new AdvancedArrayObject($value);
        }

        return $value;
    }
}
