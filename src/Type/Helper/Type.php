<?php

declare(strict_types=1);

namespace Enuage\Type\Helper;

/**
 * Interface Type
 * @package Enuage\Type\Helper
 */
interface Type
{
    const SCALAR_TYPE = 'scalar';
    const INT_TYPE = 'int';
    const INTEGER_TYPE = 'integer';
    const FLOAT_TYPE = 'float';
    const DOUBLE_TYPE = 'double';
    const STRING_TYPE = 'string';
    const STRING_INTEGER_TYPE = 'string|integer';
    const ARRAYOBJECT_TYPE = 'arrayobject';
    const ARRAY_OBJECT_TYPE = 'array|object';
    const ARRAY_TYPE = 'array';
    const OBJ_TYPE = 'object';
    const OBJECT_TYPE = 'object';
    const CALLABLE_TYPE = 'callable';
    const ITERABLE_TYPE = 'iterable';
    const RESOURCE_TYPE = 'resource';
    const NULL_TYPE = 'null';
    const MIX_TYPE = 'mixed';
    const MIXED_TYPE = 'mixed';
    const NUMBER_TYPE = 'number';
    const CALLBACK_TYPE = 'callback';
    const BOOL_TYPE = 'bool';
    const BOOLEAN_TYPE = 'boolean';
    const COUNTABLE_TYPE = 'countable';
    const JSON_TYPE = 'json';

    const TYPES = [
        self::SCALAR_TYPE,
        self::INT_TYPE,
        self::INTEGER_TYPE,
        self::FLOAT_TYPE,
        self::DOUBLE_TYPE,
        self::STRING_TYPE,
        self::STRING_INTEGER_TYPE,
        self::ARRAY_OBJECT_TYPE,
        self::ARRAY_TYPE,
        self::OBJ_TYPE,
        self::OBJECT_TYPE,
        self::CALLABLE_TYPE,
        self::ITERABLE_TYPE,
        self::RESOURCE_TYPE,
        self::NULL_TYPE,
        self::MIX_TYPE,
        self::MIXED_TYPE,
        self::NUMBER_TYPE,
        self::CALLBACK_TYPE,
        self::BOOL_TYPE,
        self::BOOLEAN_TYPE,
        self::COUNTABLE_TYPE,
        self::JSON_TYPE,
    ];

    const SCALAR_TYPES = [
        self::INT_TYPE,
        self::INTEGER_TYPE,
        self::FLOAT_TYPE,
        self::DOUBLE_TYPE,
        self::STRING_INTEGER_TYPE,
        self::STRING_TYPE,
        self::BOOL_TYPE,
        self::BOOLEAN_TYPE,
    ];
}
