<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type\Validator;

use ArrayObject;
use Closure;
use Countable;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\ArrayHelper;
use Enuage\Type\Helper\Type;
use Enuage\Type\Throwable\InvalidTypeException;
use Exception;
use Traversable;

use function class_exists;
use function class_implements;
use function class_parents;
use function filter_var;
use function function_exists;
use function gettype;
use function in_array;
use function interface_exists;
use function intval;
use function is_array;
use function is_callable;
use function is_double;
use function is_float;
use function is_integer;
use function is_null;
use function is_numeric;
use function is_object;
use function is_resource;
use function is_scalar;
use function is_string;
use function json_decode;
use function json_last_error;
use function strtolower;

/**
 * Class TypeValidator
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class TypeValidator
{
    /**
     * @deprecated Please use `anyOf` instead
     *
     * @param array $types
     * @param mixed $value
     * @param bool $strict
     *
     * @return bool
     *
     * @throws InvalidTypeException
     */
    public static function isValidTypes(array $types, $value, bool $strict = false): bool
    {
        return self::anyOf($types, $value, true, $strict);
    }

    /**
     * @param string $type
     * @param mixed $value
     * @param bool $strict
     *
     * @return bool
     */
    public static function isValid(string $type, $value, bool $strict = false): bool
    {
        try {
            self::validate($type, $value, $strict);
        } catch (Exception $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param string $type
     * @param mixed $value
     * @param bool $strict
     *
     * @throws Exception
     */
    public static function validate(string $type, $value, bool $strict = false)
    {
        $isValid = false;

        switch (strtolower($type)) {
            case Type::SCALAR_TYPE:
                $isValid = is_scalar($value);
                break;
            case Type::STRING_TYPE:
                $isValid = self::isString($value);
                break;
            case Type::STRING_INTEGER_TYPE:
                $isValid = self::isString($value) || self::isInteger($value);
                break;
            case Type::NUMBER_TYPE:
                $isValid = is_numeric($value);
                break;
            case Type::INT_TYPE:
            case Type::INTEGER_TYPE:
                $isValid = self::isInteger($value);
                break;
            case Type::FLOAT_TYPE:
            case Type::DOUBLE_TYPE:
                $isValid = self::isFloat($value);
                break;
            case Type::ARRAY_OBJECT_TYPE:
                $isValid = self::isArray($value) || self::isObject($value) || $value instanceof ArrayObject;
                break;
            case Type::ARRAYOBJECT_TYPE:
                $isValid = $value instanceof ArrayObject;
                break;
            case Type::ARRAY_TYPE:
                $isValid = self::isArray($value);
                break;
            case Type::OBJ_TYPE:
            case Type::OBJECT_TYPE:
                $isValid = self::isObject($value);
                break;
            case Type::CALLABLE_TYPE:
            case Type::CALLBACK_TYPE:
                $isValid = is_callable($value);
                break;
            case Type::ITERABLE_TYPE:
                $isValid = self::isIterable($value);
                break;
            case Type::RESOURCE_TYPE:
                $isValid = is_resource($value);
                break;
            case Type::NULL_TYPE:
                $isValid = is_null($value);
                break;
            case Type::MIX_TYPE:
            case Type::MIXED_TYPE:
                $isValid = true;
                break;
            case Type::BOOL_TYPE:
            case Type::BOOLEAN_TYPE:
                $isValid = $strict ? is_bool($value) : null !== filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
            case Type::COUNTABLE_TYPE:
                $isValid = self::isCountable($value);
                break;
            case Type::JSON_TYPE:
                $isValid = self::isJson($value);
                break;
            default:
                if (class_exists($type) || interface_exists($type)) {
                    $isValid = is_object($value) && (
                            get_class($value) === $type ||
                            in_array($type, class_implements($value)) ||
                            in_array($type, class_parents($value))
                        );
                }
                break;
        }

        if (false === $isValid) {
            throw new Exception('Provided value is not of type "'.$type.'".');
        }
    }

    /**
     * @param string|mixed $value
     *
     * @return bool
     */
    protected static function isString($value): bool
    {
        return is_string($value);
    }

    /**
     * @param integer|mixed $value
     *
     * @return bool
     */
    protected static function isInteger($value): bool
    {
        return is_integer($value);
    }

    /**
     * @param float|double|mixed $value
     *
     * @return bool
     */
    protected static function isFloat($value): bool
    {
        return is_float($value) || is_double($value);
    }

    /**
     * @param array|mixed $value
     *
     * @return bool
     */
    protected static function isArray($value): bool
    {
        return is_array($value);
    }

    /**
     * @param array|mixed $value
     *
     * @return bool
     */
    protected static function isObject($value): bool
    {
        return is_object($value);
    }

    /**
     * @param iterable|array|Traversable|mixed $value
     *
     * @noinspection PhpFullyQualifiedNameUsageInspection
     *
     * @return bool
     *
     * @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection
     */
    protected static function isIterable($value): bool
    {
        if (function_exists('is_iterable')) {
            return \is_iterable($value);
        }

        return self::isArray($value) || (self::isObject($value) && $value instanceof Traversable);
    }

    /**
     * @deprecated Please use `eachOf` instead
     *
     * @param array $values
     * @param string $type
     *
     * @throws InvalidTypeException
     */
    public static function validateArray(string $type, array $values)
    {
        self::eachOf($type, $values, false, true);
    }

    /**
     * @param array|Countable|mixed $value
     *
     * @return bool
     *
     * @noinspection PhpFullyQualifiedNameUsageInspection
     * @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection
     */
    protected static function isCountable($value): bool
    {
        if (function_exists('is_countable')) {
            return \is_countable($value);
        }

        return self::isArray($value) || (self::isObject($value) && $value instanceof Countable);
    }

    /**
     * The value should be a kind of any of provided types in requirement parameter
     *
     * Example:
     * ```
     * anyOf('string', 'a') // true
     * anyOf('string', 1) // false
     * anyOf(['string', 'integer'], 1) // true
     * anyOf(['integer', 'float'], 'a') // false
     * anyOf(['integer', 'float'], ['a']) // false
     * anyOf(['integer', 'string'], [1], true, true, true) // true
     * anyOf(['integer', 'float'], ['a'], true, true, true) // false
     * ```
     *
     * @param string|array|ArrayObject $types Type or array of types
     * @param mixed $value
     * @param bool $silent Throw exception if this parameter is equal to `false`
     * @param bool $strict Validate type strictly. E.g.: `1` for boolean check will be invalid
     * @param bool $iterate Specifies whether to check types for all elements if provided value is iterable
     *
     * @return bool
     *
     * @throws Exception
     * @throws InvalidTypeException
     */
    public static function anyOf($types, $value, bool $silent = true, bool $strict = false, bool $iterate = false): bool
    {
        return self::checkTypes($types, $value, $silent, $strict, $iterate, function (int $matches) {
            return 0 !== $matches;
        });
    }

    /**
     * Each element of iterable value should be a kind of provided type
     *
     * Example:
     * ```
     * eachOf('string', ['a']) // true
     * eachOf('integer', [1]) // true
     * eachOf('string|integer', [1, 'a']) // true
     * eachOf('string', [1]) // false
     * eachOf('integer', ['a']) // false
     * ```
     *
     * @param string $type
     * @param array|Traversable $value
     * @param bool $silent Throw exception if this parameter is equal to `false`
     * @param bool $strict Validate type strictly. E.g.: `1` for boolean check will be invalid
     *
     * @return bool
     *
     * @throws Exception
     * @throws InvalidTypeException
     */
    public static function eachOf(string $type, $value, bool $silent = true, bool $strict = false): bool
    {
        if(false === self::isIterable($value)) {
            throw new Exception('The value should be iterable.');
        }

        $value = ArrayHelper::toArray($value, true);

        return self::checkTypes($type, $value, $silent, $strict, true, function (int $matches) use ($value) {
            return $value->count() === $matches;
        });
    }

    /**
     * @param $types
     * @param $value
     * @param bool $silent
     * @param bool $strict
     * @param bool $iterate
     * @param Closure $callback
     *
     * @return bool
     *
     * @throws Exception
     * @throws InvalidTypeException
     */
    protected static function checkTypes($types, $value, bool $silent, bool $strict, bool $iterate, Closure $callback): bool
    {
        if (is_string($types)) {
            $requiredTypes = new AdvancedArrayObject([$types]);
        } elseif (self::isIterable($types)) {
            $requiredTypes = AdvancedArrayObject::fromArray($types);
        } else {
            throw new Exception('Invalid requirement provided.');
        }

        $matches = 0;
        $requiredTypes->iterate(function ($type) use ($value, $strict, $iterate, &$matches) {
            if(false === self::isIterable($value)) {
                $matches += intval(self::isValid($type, $value, $strict));

                return;
            }

            if(true === $iterate) {
                if(false === $value instanceof AdvancedArrayObject) {
                    $value = ArrayHelper::toArray($value, true);
                }

                foreach ($value->getIterator() as $item) {
                    $matches += intval(self::isValid($type, $item, $strict));
                }
            } elseif (self::isValid($type, $value, $strict)) {
                $matches++;
            }
        });

        $isValid = $callback($matches);
        if (false === $isValid && false === $silent) {
            throw new InvalidTypeException(sprintf(
                'Provided value does not correspond requirement. Value type: "%s". Required types: "%s".',
                gettype($value),
                $requiredTypes->implode('", "')
            ));
        }

        return $isValid;
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    protected static function isJson($value): bool
    {
        if(false === self::isString($value)) {
            return false;
        }

        json_decode($value);

        return JSON_ERROR_NONE === json_last_error();
    }
}
