<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type;

use InvalidArgumentException;
use function array_map;
use function get_class_methods;
use function implode;
use function in_array;
use function is_array;
use function is_object;
use function lcfirst;
use function preg_split;
use function str_replace;
use function strcmp;
use function strlen;
use function strtolower;
use function strval;
use function substr_compare;
use function ucfirst;

/**
 * Class StringObject
 *
 * @package Enuage\Type
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class StringObject
{
    const MODE_CAMEL_CASE_PASCAL = 'pascal';
    const MODE_CAMEL_CASE_DEFAULT = 'default';
    const MODE_REMOVE_EMPTY_VALUES = 1;

    const EMPTY_VALUE = '';

    /** @var string */
    private $value;

    /** @var string */
    private $initialValue;

    /**
     * StringType constructor.
     *
     * @param mixed|string $value
     */
    public function __construct($value = self::EMPTY_VALUE)
    {
        if (is_array($value)) {
            throw new InvalidArgumentException(
                'Array can not be converted to string.'
            );
        }

        if (is_object($value)) {
            if (!in_array('__toString', get_class_methods($value))) {
                throw new InvalidArgumentException(
                    'Object can not be converted to string.'
                );
            }

            $value = $value->__toString();
        }

        $value = strval($value);

        $this->value = $value;
        $this->initialValue = $value;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

    public function getValue(): string
    {
        return $this->value;
    }

    protected function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function toSnakeCase(): string
    {
        return implode('_', $this->transformPrepare());
    }

    /**
     * @return array|string[]
     */
    private function transformPrepare(): array
    {
        $parts = preg_split('/((?=[A-Z])|([_\-\s]))/', $this->getValue());

        foreach ($parts as $index => &$part) {
            if (empty($part)) {
                unset($parts[$index]);
                continue;
            }

            $part = strtolower($part);
        }

        return $parts;
    }

    public function toKebabCase(): string
    {
        $stringParts = $this->transformPrepare();

        return implode('-', $stringParts);
    }

    public function toCamelCase(
        string $mode = self::MODE_CAMEL_CASE_DEFAULT
    ): string {
        $stringParts = $this->transformPrepare();

        array_map(
            function (string &$part) {
                $part = ucfirst($part);
            },
            $stringParts
        );

        $result = implode('', $stringParts);

        if (self::MODE_CAMEL_CASE_PASCAL !== $mode) {
            $result = lcfirst($result);
        }

        return $result;
    }

    public function prepend(string $prefix): self
    {
        $this->setValue($prefix.$this->getValue());

        return $this;
    }

    public function isEndsWith(string $end): bool
    {
        if ($this->isEmpty()) {
            return false;
        }

        return 0 === substr_compare($this->getValue(), $end, -strlen($end));
    }

    public function isEmpty(): bool
    {
        return 0 === strlen($this->getValue());
    }

    public function isStartsWith(string $start): bool
    {
        if ($this->isEmpty()) {
            return false;
        }

        return 0 === substr_compare($this->getValue(), $start, 0, strlen($start));
    }

    public function isEqualTo(string $value): bool
    {
        return 0 === strcmp($this->getValue(), $value);
    }

    public function reset(): self
    {
        $this->setValue($this->getInitialValue());

        return $this;
    }

    public function getInitialValue(): string
    {
        return $this->initialValue;
    }

    public function append(string $appendix): self
    {
        $this->setValue($this->getValue() . $appendix);

        return $this;
    }

    /**
     * @see str_replace()
     * @link https://php.net/manual/en/function.str-replace.php
     */
    public function replace($search, $replace): self
    {
        $this->setValue(str_replace($search, $replace, $this->getValue()));

        return $this;
    }

    /**
     * @see explode()
     * @link https://php.net/manual/en/function.explode.php
     */
    public function explode(
        string $delimiter,
        int $mode = 0,
        int $limit = PHP_INT_MAX
    ): AdvancedArrayObject {
        $result = new AdvancedArrayObject();
        if (!$this->isEmpty()) {
            $result = AdvancedArrayObject::explode(
                $delimiter,
                $this->getValue(),
                $limit
            );
        }

        if (self::MODE_REMOVE_EMPTY_VALUES === $mode) {
            $result->iterate(function ($value, $key) use ($result) {
                if (empty($value)) {
                    $result->removeByKey($key);
                }
            });
        }

        return $result;
    }
}
