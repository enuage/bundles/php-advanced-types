<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type;

use ArrayObject;
use Enuage\Type\Helper\ArrayHelper;
use Exception;

use Traversable;

use function array_values;

/**
 * Class PseudoEnum
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class PseudoEnum
{
    /**
     * @var AdvancedArrayObject
     */
    protected $elements;

    /**
     * EnumType constructor.
     *
     * @param array|ArrayObject $elements
     *
     * @throws Exception
     */
    public function __construct($elements)
    {
        if (!ArrayHelper::isIterable($elements)) {
            throw new Exception('Invalid value provided as enum elements.');
        }

        if ($elements instanceof ArrayObject) {
            $elements = $elements->getArrayCopy();
        }

        $this->elements = new AdvancedArrayObject(array_values($elements));
    }

    /**
     * @return AdvancedArrayObject
     */
    public function getElements(): AdvancedArrayObject
    {
        return $this->elements;
    }

    /**
     * @param array|ArrayObject|Traversable $elements
     *
     * @return bool
     *
     * @throws Exception
     */
    public function validate($elements)
    {
        if (!ArrayHelper::isIterable($elements)) {
            throw new Exception('Invalid value provided for validation.');
        }

        if ($elements instanceof ArrayObject) {
            $elements = $elements->getArrayCopy();
        }

        if (!$this->elements->getDifference($elements)->isEmpty()) {
            throw new Exception('Provided array does not contains required elements.');
        }

        return true;
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function containsValue($value): bool
    {
        try {
            return $this->validateValue($value);
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @param $value
     *
     * @return bool
     *
     * @throws Exception
     */
    public function validateValue($value)
    {
        if (!$this->elements->containsElement($value)) {
            throw new Exception('Provided array does not contains provided element.');
        }

        return true;
    }

    /**
     * @param array|ArrayObject $elements
     *
     * @return PseudoEnum
     *
     * @throws Exception
     */
    public static function fromArray($elements): PseudoEnum
    {
        return new self($elements);
    }
}
