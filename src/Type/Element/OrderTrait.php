<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type\Element;

use function intval;

/**
 * Trait OrderTrait
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
trait OrderTrait
{
    /**
     * @var int
     */
    protected $orderNumber = 0;

    /**
     * {@inheritDoc}
     */
    public function getOrderNumber(): int
    {
        return intval($this->orderNumber);
    }

    /**
     * {@inheritDoc}
     *
     * @return self
     */
    public function setOrderNumber(int $orderNumber): self
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }
}
