<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type\Element;

/**
 * Class ChainNode
 *
 * @package Enuage\Type\Element
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class ChainNode
{
    /**
     * @var ChainNode|null
     */
    protected $previousNode;

    /**
     * @var ChainNode|null
     */
    protected $nextNode;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var string|null
     */
    protected $title;

    /**
     * ChainNode constructor.
     *
     * @param ChainNode $previousNode
     * @param $value
     */
    public function __construct($value, ChainNode $previousNode = null)
    {
        $this->value = $value;
        $this->previousNode = $previousNode;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return spl_object_hash($this);
    }

    /**
     * @return bool
     */
    public function isRoot(): bool
    {
        return null === $this->getPreviousNode();
    }

    /**
     * @return ChainNode|null
     */
    public function getPreviousNode()
    {
        return $this->previousNode;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return ChainNode
     */
    public function setTitle(string $title): ChainNode
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param callable|null $callback
     *
     * @return ChainNode
     */
    public function removeNextNode(callable $callback = null): ChainNode
    {
        if ($nextNode = $this->getNextNode()) {
            if (null !== $callback) {
                $callback($this->getNextNode());
            }

            $this->getNextNode()->removeNextNode($callback);
        }

        $this->nextNode = null;

        return $this;
    }

    /**
     * @return ChainNode|null
     */
    public function getNextNode()
    {
        return $this->nextNode;
    }

    /**
     * @param ChainNode|null $nextNode
     *
     * @return ChainNode
     */
    public function setNextNode(ChainNode $nextNode): ChainNode
    {
        $this->nextNode = $nextNode;

        return $this;
    }
}
