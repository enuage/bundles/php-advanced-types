<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type\Element;

/**
 * Interface OrderedElementInterface
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
interface OrderedElementInterface
{
    /**
     * @return int
     *
     * @return mixed
     */
    public function getOrderNumber(): int;

    /**
     * @param int $orderNumber
     *
     * @return mixed
     */
    public function setOrderNumber(int $orderNumber);
}
