<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type;

use Closure;
use Enuage\Type\Element\OrderedElementInterface;
use Exception;
use InvalidArgumentException;
use RuntimeException;
use SplQueue;

/**
 * Class QueueOrdered
 *
 * Inspired on SplPriorityQueue
 * https://www.php.net/manual/en/class.splpriorityqueue.php
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class QueueOrdered extends SplQueue
{
    const ORDER_ASC = 0;
    const ORDER_DESC = 1;

    /**
     * @var int
     */
    private $orderDirection = self::ORDER_ASC;

    /**
     * @return void
     *
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function order()
    {
        if ($this->isEmpty()) {
            return;
        }

        $elements = new AdvancedArrayObject();
        $this->iterate(function ($element) use ($elements) {
            $elements->add($element);
        });

        $elements->uasort($this->getOrderingClosure());

        $this->clear();
        $this->__construct($elements->toArray(), $this->orderDirection);
        $this->rewind();
    }

    /**
     * @param Closure $closure
     */
    public function iterate(Closure $closure)
    {
        if ($this->isEmpty()) {
            return;
        }

        $queue = clone $this;
        $queue->rewind();
        while ($queue->valid()) {
            $element = $queue->current();

            if (null !== $element) {
                $closure->call($queue, $element);
            }

            $queue->next();
        }
        unset($queue);
    }

    /**
     * @param int $orderDirection
     *
     * @return Closure
     */
    private function getOrderingClosure(int $orderDirection = null): Closure
    {
        if (null === $orderDirection) {
            $orderDirection = $this->orderDirection;
        }

        return function (OrderedElementInterface $x, OrderedElementInterface $y) use ($orderDirection) {
            if (self::ORDER_ASC === $orderDirection) {
                return $x->getOrderNumber() <=> $y->getOrderNumber();
            }

            return $y->getOrderNumber() <=> $x->getOrderNumber();
        };
    }

    /**
     * @return void
     */
    public function clear()
    {
        $this->rewind();
        while (!$this->isEmpty()) {
            $this->dequeue();
        }
    }

    /**
     * QueueOrdered constructor.
     *
     * @param OrderedElementInterface[]|array $elements
     * @param int $orderDirection
     *
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function __construct(array $elements = [], int $orderDirection = self::ORDER_ASC)
    {
        $this->setOrderDirection($orderDirection);

        if (empty($elements)) {
            return;
        }

        foreach ($elements as $element) {
            $this->enqueue($element);
        }
    }

    /**
     * @param int $orderDirection
     *
     * @return self
     */
    public function setOrderDirection(int $orderDirection): self
    {
        $this->orderDirection = $orderDirection;

        return $this;
    }

    /**
     * @param OrderedElementInterface $element
     *
     * @return void
     *
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function enqueue($element)
    {
        if (!$this->isElementValid($element)) {
            throw new InvalidArgumentException(
                'You provided invalid element for queue. Instance of "' . OrderedElementInterface::class . '" is expected.'
            );
        }

        if (0 === $element->getOrderNumber()) {
            $element->setOrderNumber($this->getMaxOrderNumber() + 1);
        }

        parent::enqueue($element);
    }

    /**
     * @param OrderedElementInterface|mixed $element
     *
     * @return bool
     */
    protected function isElementValid($element): bool
    {
        return $element instanceof OrderedElementInterface;
    }

    /**
     * @return int
     *
     * @throws Exception
     * @throws RuntimeException
     */
    public function getMaxOrderNumber(): int
    {
        $result = 0;
        if ($this->isEmpty()) {
            return $result;
        }

        $this->iterate(function ($element) use (&$result) {
            if (!$this->isElementValid($element)) {
                throw new RuntimeException(
                    'Queue contains invalid value. Instance of "' . OrderedElementInterface::class . '" is expected.'
                );
            }

            /** @var OrderedElementInterface $element */
            if ($result < $element->getOrderNumber()) {
                $result = $element->getOrderNumber();
            }
        });

        return $result;
    }
}
