<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type;

use ArrayObject;
use Closure;
use Countable;
use Enuage\Type\Helper\ArrayHelper;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;
use Iterator;
use RuntimeException;
use Traversable;

use function array_diff;
use function array_filter;
use function array_keys;
use function array_map;
use function array_search;
use function array_slice;
use function array_values;
use function count;
use function end;
use function explode;
use function implode;
use function in_array;
use function is_integer;
use function is_numeric;
use function iterator_to_array;
use function key;
use function max;
use function next;
use function spl_object_hash;
use function strval;

/**
 * Class AdvancedArrayObject
 *
 * Inspired on Doctrine's ArrayCollection
 * https://github.com/doctrine/collections/blob/master/lib/Doctrine/Common/Collections/ArrayCollection.php
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class AdvancedArrayObject extends ArrayObject
{
    const MODE_CONTAINS_STRICT = 'strict';
    const MODE_CONTAINS_EASY = 'easy';

    /**
     * Create new example of `ArrayType` object from iterator.
     *
     * This method can be used for example if you'd modified array through iterator and want to work with it as with
     * array in future
     *
     * @param Iterator $iterator
     *
     * @return AdvancedArrayObject
     */
    public static function fromIterator(Iterator $iterator): AdvancedArrayObject
    {
        return new self(iterator_to_array($iterator));
    }

    /**
     * Explode string to the array of parts by delimiter.
     *
     * @param string $delimiter
     * @param string $input
     * @param int $limit
     *
     * @return AdvancedArrayObject
     */
    public static function explode(string $delimiter, string $input, int $limit = PHP_INT_MAX): AdvancedArrayObject
    {
        return new self(explode($delimiter, $input, $limit) ?: []);
    }

    /**
     * Allow to import elements from array with replacing keys if you want
     *
     * You can provide which elements do you want to import, which of their keys should be replaced or keep two last
     * properties empty for creating new ArrayType object from provided array. Note, that method is position sensitive,
     * and element with position `N` in `$keys` array will replace key of provided array element in position `N` if you
     * provide this key in `$importKeys` array
     *
     * Example:
     * ```php
     * $array = ArrayType::fromArray(['a' => 1, 'b' => 2, 'c' => 3], ['a', 'b', 'c'], ['x', 'y', 'z']);
     * var_dump($example); // ['x' => 1, 'y' => 2, 'z' => 3]
     *
     * $array2 = ArrayType::fromArray(['a' => 1, 'b' => 2, 'c' => 3], ['a', 'b'], ['x', 'y', 'z']);
     * var_dump($example); // ['x' => 1, 'y' => 2]
     *
     * $array3 = ArrayType::fromArray(['a' => 1, 'b' => 2, 'c' => 3], ['a', 'b', 'c'], ['x', 'y']);
     * var_dump($example); // ['x' => 1, 'y' => 2, 'c' => 3]
     * ```
     *
     * @param array $elements
     * @param array $importKeys
     * @param array $keys
     *
     * @return AdvancedArrayObject
     *
     * @throws Exception
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public static function fromArray($elements, array $importKeys = [], array $keys = []): AdvancedArrayObject
    {
        if (!ArrayHelper::isIterable($elements)) {
            throw new RuntimeException('Provided argument for method "import()" is not iterable.');
        }

        $elements = ArrayHelper::toArray($elements, true);

        if (empty($importKeys)) {
            return new self($elements);
        }

        $importKeys = array_values($importKeys);
        $keys = array_values($keys);

        $result = new self();
        $keysCount = count($keys) - 1;
        foreach ($importKeys as $index => $key) {
            if ($elements->containsKey($key)) {
                $newKey = !empty($keys) && $index <= $keysCount ? $keys[$index] : $key;
                $result->set($newKey, $elements->get($key));
            }
        }

        return new self($result);
    }

    /**
     * Check if array contains key.
     *
     * @param int|string $key
     *
     * @return bool
     *
     * @throws Exception
     */
    public function containsKey($key): bool
    {
        TypeValidator::validate(Type::STRING_INTEGER_TYPE, $key);

        return $this->offsetExists($key);
    }

    /**
     * Sets an element in the array at the specified key/index.
     *
     * @param mixed $key
     * @param mixed $value
     *
     * @return AdvancedArrayObject
     */
    public function set($key, $value): AdvancedArrayObject
    {
        $this->offsetSet($key, $value);

        return $this;
    }

    /**
     * Returns element by $key of $default value.
     *
     * @param int|string $key
     * @param null|mixed $default
     *
     * @return null|mixed
     *
     * @throws Exception
     */
    public function get($key, $default = null)
    {
        return $this->containsKey($key) ? $this->offsetGet($key) : $default;
    }

    /**
     * Get string representation of object.
     *
     * @return string
     */
    public function __toString(): string
    {
        return self::class.'@'.spl_object_hash($this);
    }

    /**
     * Convert object to base PHP array type.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->getArrayCopy();
    }

    /**
     * Get first array element.
     *
     * @return mixed
     */
    public function first()
    {
        $this->getIterator()->rewind();

        return $this->getIterator()->current();
    }

    /**
     * Get last array element.
     *
     * @return mixed
     */
    public function last()
    {
        $elements = iterator_to_array($this->getIterator());

        return end($elements);
    }

    /**
     * Gets the key/index of the element at the current iterator position.
     *
     * @return int|string|null
     */
    public function key()
    {
        return key($this);
    }

    /**
     * Moves the internal iterator position to the next element and returns this element.
     *
     * @return mixed
     */
    public function next()
    {
        return next($this);
    }

    /**
     * Gets the element of the array at the current iterator position.
     *
     * @return mixed
     */
    public function current()
    {
        return $this->getIterator()->current();
    }

    /**
     * Remove elements from the array.
     *
     * You can list elements delimited by comma: `removeElements(x, y)` or give as array of elements:
     * `removeElements(...[x, y])`
     *
     * If you want to reset keys after removing elements, use `resetKeys(void):ArrayType` method
     *
     * @param mixed ...$elements
     *
     * @return AdvancedArrayObject
     *
     * @throws Exception
     */
    public function removeElements(...$elements): AdvancedArrayObject
    {
        foreach ($elements as $element) {
            $this->removeElement($element);
        }

        return $this;
    }

    /**
     * Remove single element from the array.
     *
     * @param $element
     * @param bool $resetKeys
     *
     * @return AdvancedArrayObject
     *
     * @throws Exception
     */
    public function removeElement($element, bool $resetKeys = false): AdvancedArrayObject
    {
        $key = array_search($element, $this->getArrayCopy(), true);
        if (false !== $key) {
            $this->removeByKey($key);
        }

        if ($resetKeys) {
            $this->resetKeys();
        }

        return $this;
    }

    /**
     * Remove single element by key.
     *
     * @param null|int|string $key
     * @param bool $resetKeys
     *
     * @return AdvancedArrayObject
     *
     * @throws Exception
     */
    public function removeByKey($key, bool $resetKeys = false): AdvancedArrayObject
    {
        TypeValidator::validate(Type::STRING_INTEGER_TYPE, $key);

        if ($this->offsetExists($key)) {
            $this->offsetUnset($key);
        }

        if ($resetKeys) {
            $this->resetKeys();
        }

        return $this;
    }

    /**
     * Reset keys of the array.
     *
     * @return AdvancedArrayObject
     */
    public function resetKeys(): AdvancedArrayObject
    {
        $this->recreate(array_values($this->getArrayCopy()));

        return $this;
    }

    /**
     * Recreate array with different set of values.
     *
     * @param array $elements
     *
     * @return AdvancedArrayObject
     */
    public function recreate(array $elements): AdvancedArrayObject
    {
        $this->__construct($elements, $this->getFlags(), $this->getIteratorClass());

        return $this;
    }

    /**
     * Remove elements by key.
     *
     * You can list keys delimited by comma: `removeByKeys(x, y)` or give as array of keys: `removeByKeys(...[x, y])`
     *
     * @param mixed ...$keys
     *
     * @return AdvancedArrayObject
     *
     * @throws Exception
     */
    public function removeByKeys(...$keys): AdvancedArrayObject
    {
        foreach ($keys as $key) {
            TypeValidator::validate(Type::STRING_INTEGER_TYPE, $key);

            $this->removeByKey($key);
        }

        return $this;
    }

    /**
     * Iterate array object.
     *
     * First parameter of the callback is value, second is key.
     *
     * Usage:
     *
     * ```php
     *      AdvancedArrayObject::fromArray([])->iterate(function($value, $key) {});
     * ```
     *
     * @param Closure $callback
     *
     * @return void
     */
    public function iterate(Closure $callback)
    {
        array_map($callback, $this->getValues(), $this->getKeys());
    }

    /**
     * Applies the given function to each element in the array.
     *
     * @param Closure $callback
     * @param bool $updateSelf
     *
     * @return AdvancedArrayObject
     */
    public function map(Closure $callback, bool $updateSelf = false): AdvancedArrayObject
    {
        $elements = array_map($callback, $this->getArrayCopy());

        if ($updateSelf) {
            return $this->recreate($elements);
        }

        return new self($elements);
    }

    /**
     * Check if array contains keys.
     *
     * You can list keys delimited by comma: `containsKeys(x, y)` or give as array of keys: `containsKeys(...[x, y])`
     *
     * @param int|string ...$keys
     *
     * @return bool
     *
     * @throws Exception
     */
    public function containsKeys(...$keys): bool
    {
        foreach ($keys as $key) {
            if (false === $this->containsKey($key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if array contains elements.
     *
     * You can list elements delimited by comma: `containsElements(x, y)` or give as array of elements:
     * `containsElements(...[x, y])`
     *
     * Mode `strict` will check if array object should contains all provided elements, whereas mode `easy` will only
     * check if at least one of elements exists.
     *
     * @param mixed ...$elements
     * @param string $mode
     *
     * @return bool
     */
    public function containsElements(array $elements, string $mode = self::MODE_CONTAINS_STRICT): bool
    {
        $foundElementsCount = 0;
        foreach ($elements as $element) {
            if (true === $this->containsElement($element)) {
                $foundElementsCount++;
            }
        }

        if (self::MODE_CONTAINS_EASY === $mode) {
            return $foundElementsCount > 0;
        }

        return count($elements) === $foundElementsCount;
    }

    /**
     * Check if array contains element.
     *
     * @param $element
     *
     * @return bool
     */
    public function containsElement($element): bool
    {
        return in_array($element, $this->getArrayCopy(), true);
    }

    /**
     * Tests for the existence of an element that satisfies the given predicate.
     *
     * @param Closure $predicate
     *
     * @return bool
     */
    public function exists(Closure $predicate): bool
    {
        foreach ($this->getArrayCopy() as $key => $value) {
            if ($predicate($value, $key)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get values by list of keys or all values of the array.
     *
     * You can list keys delimited by comma: `getValues(x, y)` or give as array of keys: `getValues(...[x, y])`
     *
     * @param array|string[]|int[] ...$keys
     *
     * @return array
     *
     * @throws Exception
     */
    public function getKeysValues(...$keys): array
    {
        $elements = [];

        /** @var int|string $key */
        foreach ($keys as $key) {
            if ($this->containsKey($key)) {
                $elements[] = $this->offsetGet($key);
            }
        }

        return array_values($elements);
    }

    /**
     * Add list of values.
     *
     * You can add values delimited by comma: `add(x, y)` or add as array of values: `add(...[x, y])`
     *
     * @param mixed ...$elements
     *
     * @return AdvancedArrayObject
     *
     * @throws Throwable\InvalidTypeException
     */
    public function add(...$elements): AdvancedArrayObject
    {
        foreach ($elements as $element) {
            $this->append($element);
        }

        return $this;
    }

    /**
     * @param Traversable|mixed $element
     * @param bool $isRecursive
     *
     * @return AdvancedArrayObject
     *
     * @throws Throwable\InvalidTypeException
     *
     * @noinspection PhpParameterNameChangedDuringInheritanceInspection
     */
    public function append($element, bool $isRecursive = false): AdvancedArrayObject
    {
        if (!$isRecursive) {
            parent::append($element);
        }

        if (ArrayHelper::isIterable($element)) {
            foreach ($element as $value) {
                $this->append(
                    $value,
                    $isRecursive && ArrayHelper::isIterable($value)
                );
            }
        }

        return $this;
    }

    /**
     * Returns all the elements of this collection that satisfy the predicate.
     *
     * @param callable $callback
     * @param bool $updateSelf
     * @param int $flag
     *
     * @return AdvancedArrayObject
     */
    public function filter(
        callable $callback,
        bool $updateSelf = false,
        int $flag = ARRAY_FILTER_USE_BOTH
    ): AdvancedArrayObject {
        $elements = array_filter($this->getArrayCopy(), $callback, $flag);

        if ($updateSelf) {
            return $this->recreate($elements);
        }

        return new self($elements);
    }

    /**
     * Tests whether the given predicate holds for all elements of this array.
     *
     * @param Closure $predicate
     *
     * @return bool
     */
    public function isTrueForAll(Closure $predicate): bool
    {
        return ArrayHelper::isTrueForAll($this, $predicate);
    }

    /**
     * Clears the array, removing all elements.
     *
     * @return AdvancedArrayObject
     */
    public function clear(): AdvancedArrayObject
    {
        $this->__construct([], $this->getFlags(), $this->getIteratorClass());

        return $this;
    }

    /**
     * Extracts a slice of $limit elements starting at position $offset from the array.
     *
     * @param int $offset
     * @param int|null $limit
     * @param bool $updateSelf
     *
     * @return AdvancedArrayObject
     */
    public function slice(int $offset = 0, int $limit = null, bool $updateSelf = false): AdvancedArrayObject
    {
        $slice = array_slice($this->getArrayCopy(), $offset, $limit, true);

        if ($updateSelf) {
            return $this->recreate($slice);
        }

        return new self($slice);
    }

    /**
     * Returns next element from provided key.
     *
     * @param mixed $current
     *
     * @return null|mixed
     */
    public function getNext($current)
    {
        $iterator = $this->getIterator();
        while ($iterator->valid()) {
            if ($current === $iterator->key()) {
                $iterator->next();

                return $iterator->current();
            }

            $iterator->next();
        }

        return null;
    }

    /**
     * Returns string representation of the element by $key of $default value.
     *
     * @param $key
     * @param null|string $default
     *
     * @return null|string
     *
     * @throws Exception
     */
    public function getStringValue($key, string $default = null)
    {
        return $this->containsKey($key) ? strval($this->get($key)) : $default;
    }

    /**
     * Returns integer representation of the element by $key of $default value.
     *
     * @param $key
     * @param int|null $default
     *
     * @return int|null
     *
     * @throws Exception
     */
    public function getIntValue($key, int $default = null)
    {
        return $this->containsKey($key) ? filter_var($this->get($key), FILTER_VALIDATE_INT) : $default;
    }

    /**
     * Returns float representation of the element by $key of $default value.
     *
     * @param $key
     * @param float|null $default
     *
     * @return float|null
     *
     * @throws Exception
     */
    public function getFloatValue($key, float $default = null)
    {
        return $this->containsKey($key) ? filter_var($this->get($key), FILTER_VALIDATE_FLOAT) : $default;
    }

    /**
     * Returns boolean representation of the element by $key of $default value.
     *
     * @param $key
     * @param bool|null $default
     *
     * @return bool
     *
     * @throws Exception
     */
    public function getBooleanValue($key, bool $default = false): bool
    {
        return $this->containsKey($key) ? filter_var($this->get($key), FILTER_VALIDATE_BOOLEAN) : $default;
    }

    /**
     * Check if value of the $key is empty.
     *
     * @param $key
     *
     * @return bool
     * @throws Exception
     *
     */
    public function isEmptyValue($key): bool
    {
        if ($this->containsKey($key)) {
            $value = $this->offsetGet($key);

            if (null === $value) {
                return true;
            }

            if (is_string($value)) {
                return '' === $value;
            }

            if (is_array($value)) {
                return empty($value);
            }

            if (is_object($value)) {
                if ($value instanceof Countable) {
                    return 0 === $value->count();
                }

                return empty(ArrayHelper::toArray($value));
            }

            return false;
        }

        return true;
    }

    /**
     * @param array $elements
     * @param bool $asObject
     *
     * @return array|AdvancedArrayObject
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function getDifference($elements, bool $asObject = true)
    {
        $difference = array_diff($elements, $this->getArrayCopy());
        $difference = array_values($difference);

        if (true === $asObject) {
            $difference = new self($difference);
        }

        return $difference;
    }

    /**
     * @param string $glue
     * @param bool $asObject Convert result to `AdvancedArrayObject`
     *
     * @return string|StringObject
     */
    public function implode(string $glue, bool $asObject = false)
    {
        $result = implode($glue, $this->getValues());
        if (true === $asObject) {
            return new StringObject($result);
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return array_values($this->getArrayCopy());
    }

    /**
     * @param $elements
     * @param bool $replace
     *
     * @return self
     *
     * @throws Exception
     */
    public function import($elements, bool $replace = true): self
    {
        $elements = ArrayHelper::toArray($elements, true);
        foreach ($elements->getIterator() as $key => $value) {
            if (is_numeric($key) && false === $replace) {
                $this->push($value);
            } else {
                if (false === $replace && $this->containsKey($key)) {
                    continue;
                }

                $this->set($key, $value);
            }
        }

        return $this;
    }

    /**
     * @param mixed $element
     *
     * @return self
     */
    public function push($element): self
    {
        $this->set(1 + $this->getMaxKey(true), $element);

        return $this;
    }

    /**
     * @param bool $forceInt
     *
     * @return int|string
     */
    public function getMaxKey(bool $forceInt = false)
    {
        if ($this->isEmpty()) {
            return 0;
        }

        $maxKey = max($this->getKeys());
        if (!is_integer($maxKey) && $forceInt) {
            $maxKey = 0;
        }

        return $maxKey;
    }

    /**
     * Checks if array is empty.
     *
     * @return bool
     */
    public function isEmpty(): bool
    {
        return 0 === $this->count();
    }

    /**
     * Gets all keys/indices of the array.
     *
     * @return array
     */
    public function getKeys(): array
    {
        return array_keys($this->getArrayCopy());
    }

    /**
     * @param int|string $key
     * @param array|int[]|string[] $newKeys
     *
     * @return AdvancedArrayObject
     *
     * @throws Exception
     */
    public function duplicateValueForKeys($key, array $newKeys): AdvancedArrayObject
    {
        foreach ($newKeys as $newKey) {
            $this->duplicateValueForKey($key, $newKey);
        }

        return $this;
    }

    /**
     * @param int|string $key
     * @param int|string $newKey
     *
     * @return AdvancedArrayObject
     *
     * @throws Exception
     */
    public function duplicateValueForKey($key, $newKey): AdvancedArrayObject
    {
        TypeValidator::validate(Type::STRING_INTEGER_TYPE, $key);
        TypeValidator::validate(Type::STRING_INTEGER_TYPE, $newKey);

        if ($this->containsKey($key)) {
            $this->set($newKey, $this->get($key));
        }

        return $this;
    }
}
