<?php
/**
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code. Or visit
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 */

declare(strict_types=1);

namespace Enuage\Type;

use Enuage\Type\Helper\ArrayHelper;
use Enuage\Type\Helper\Type;
use Enuage\Type\Throwable\InvalidTypeException;
use Enuage\Type\Validator\TypeValidator;
use Exception;

/**
 * Class PseudoGeneric
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class PseudoGeneric extends AdvancedArrayObject
{
    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var string $keyType
     */
    protected $keyType;

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public static function explode(string $delimiter, string $input, int $limit = PHP_INT_MAX): AdvancedArrayObject
    {
        return new self(Type::STRING_TYPE, Type::INTEGER_TYPE, explode($delimiter, $input, $limit) ?: []);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function removeElement($element, bool $resetKeys = false): AdvancedArrayObject
    {
        $this->validateValueType($element);

        return parent::removeElement($element, $resetKeys);
    }

    /**
     * @param mixed $value
     *
     * @throws InvalidTypeException
     */
    protected function validateValueType($value)
    {
        TypeValidator::anyOf($this->type, $value, false, true);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function recreate(array $elements): AdvancedArrayObject
    {
        $this->__construct($this->type, $this->keyType, $elements, $this->getFlags(), $this->getIteratorClass());

        return $this;
    }

    /**
     * GenericType constructor.
     *
     * @param string $type
     * @param string $keyType
     *
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function __construct(
        string $type = Type::SCALAR_TYPE,
        string $keyType = Type::INTEGER_TYPE,
        $input = array(),
        $flags = 0,
        $iterator_class = "ArrayIterator"
    )
    {
        $keyTypes = new PseudoEnum([
            Type::STRING_TYPE,
            Type::INTEGER_TYPE,
            Type::STRING_INTEGER_TYPE,
        ]);

        if (Type::SCALAR_TYPE === $keyType) {
            $keyType = Type::STRING_INTEGER_TYPE;
        }

        if (!$keyTypes->containsValue($keyType)) {
            throw new Exception('Unsupported key type provided. It must be either "string" or "integer".');
        }

        $this->type = $type;
        $this->keyType = $keyType;

        ArrayHelper::isTrueForAll($input, function ($value, $key) {
            $this->validateKeyType($key);
            $this->validateValueType($value);
        });

        parent::__construct($input, $flags, $iterator_class);
    }

    /**
     * @param int|string $key
     *
     * @throws InvalidTypeException
     */
    protected function validateKeyType($key)
    {
        TypeValidator::anyOf($this->keyType, $key, false, true);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function removeByKey($key, bool $resetKeys = false): AdvancedArrayObject
    {
        $this->validateKeyType($key);

        return parent::removeByKey($key, $resetKeys);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function containsKey($key): bool
    {
        $this->validateKeyType($key);

        return parent::containsKey($key);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function containsElement($element): bool
    {
        $this->validateValueType($element);

        return parent::containsElement($element);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function set($key, $value): AdvancedArrayObject
    {
        $this->validateKeyType($key);
        $this->validateValueType($value);

        return parent::set($key, $value);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function append($element, bool $isRecursive = false): AdvancedArrayObject
    {
        if (!is_array($element)) {
            $this->validateValueType($element);
        }

        return parent::append($element, $isRecursive);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function getNext($current)
    {
        $this->validateKeyType($current);

        return parent::getNext($current);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function get($key, $default = null)
    {
        $this->validateKeyType($key);
        if (null !== $default) {
            $this->validateValueType($default);
        }

        return parent::get($key, $default);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function push($element): AdvancedArrayObject
    {
        $this->validateValueType($element);

        return parent::push($element);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function getDifference($elements, bool $asObject = true)
    {
        ArrayHelper::isTrueForAll($elements, function ($value) {
            $this->validateValueType($value);
        });

        $difference = parent::getDifference($elements, true);

        if (true === $asObject) {
            return new self($this->type, $this->keyType, $difference);
        }

        return $difference->getArrayCopy();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return self::class . '@' . spl_object_hash($this);
    }
}
