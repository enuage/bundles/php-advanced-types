<?php

declare(strict_types=1);

namespace Enuage\Type\Throwable;

use Exception;

class InvalidTypeException extends Exception
{
}
